package t1.dkhrunina.tm.api.service;

import t1.dkhrunina.tm.api.repository.IProjectRepository;
import t1.dkhrunina.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

}