package t1.dkhrunina.tm.api.service;

import t1.dkhrunina.tm.api.repository.ITaskRepository;
import t1.dkhrunina.tm.model.Task;

public interface ITaskService extends ITaskRepository {

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

}